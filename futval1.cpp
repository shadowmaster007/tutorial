#include <iostream>
#include <cmath>
using namespace std;

double future_value(double rate, double years)
{
	double b = 1000*pow(1+rate/100,years);
	return b;
}
int main()
{
	cout << "Please enter the interest rate in percent: ";
	double rate;
	cin >> rate;
	cout << "Please enter years: ";
	double years;
	cin >> years;
	
	double balance = future_value(rate,years);
	cout << "After " << years << ",the balance is " << balance << endl;
	return 0;
}
