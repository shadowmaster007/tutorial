#include <iostream>
#include <string>
using namespace std;

void do_the_thing(const string& str)
{
    for(int i=str.length()-1; i>=0; i--)
    {
        cout << str[i];
    }
    cout << endl;
}

int main()
{
	string s;
	while (cin >> s)
	{
		if (s == "exit")
		{
			break;
		}

		do_the_thing(s);
	}

	return 0;
}
