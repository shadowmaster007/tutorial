#include <iostream>
#include <cmath>
#include <assert.h>
using namespace std;

double future_value(double initial_balance, double p, int nyear)
{
	assert(nyear>= 0 && p>=0);
	return initial_balance * pow(1+ p/(12*100),12*nyear);
}
int main()
{
	cout << future_value (1000,6,-5);
	return 0;
}
