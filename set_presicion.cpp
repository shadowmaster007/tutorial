#include <iostream>
#include <string>
#include <iomanip>
using namespace std;
int main()
{   cout << "Enter pennies: ";
	int pennies;
	cin >> pennies;
	cout << "Enter nickels: ";
	int nickels;
	cin >> nickels;
	cout << "Enter dimes: ";
	int dimes;
	cin>> dimes;
	cout << "Enter quarters: ";
	int quarters;
	cin >> quarters;
	
	cout << fixed << setprecision(2);
	cout << setw(8) << pennies << " " << setw(8) << pennies*0.01 << endl;
	cout << setw(8) << nickels << " " << setw(8) << nickels*0.05 << endl;
	cout << setw(8) << dimes << " " << setw(8) << dimes*0.10 << endl;
	cout << setw(8) << quarters << " " << setw(8) << quarters*0.25 << endl;
	return 0;
 }
