#include <iostream>
#include <string>
using namespace std;

string digit_name(int n)

{
	if (n==1) return "one";
	else if (n==2) return "two";
	else if (n==3) return "three";
	else if (n==4) return "four";
	else if (n==5) return "five";
	else if (n==6) return "six";
    else if (n==7) return "seven";
	else if (n==8) return "eight";
	else if (n==9) return "nine";
	return "";
}
string teen_name(int n)
{
	if (n==10) return "ten";
	else if (n==11) return "eleven";
	else if (n==12) return "twelve";
	else if (n==13) return "threeteen";
	else if (n==14) return "fourteen";
	else if (n==15) return "fiveteen";
	else if (n==16) return "sixteen";
    else if (n==17) return "seventeen";
	else if (n==18) return "eightteen";
	else if (n==19) return "nineteen";
	return "";
}
string tens_name (int n)
{
	
	if (n==2) return "twenty";
	else if (n==3) return "thirty";
	else if (n==4) return "forty";
	else if (n==5) return "fifty";
	else if (n==6) return "sixty";
    else if (n==7) return "seventy";
	else if (n==8) return "eighty";
	else if (n==9) return "ninety";
	return "";
}
string int_name (int n)
{
	int c=n;
	string r;
	if (c >=1000)
	{
		r = int_name(c/1000) + " thousand";
		c=c%1000;
	}
	if (c >= 100)
	{
		r = r + " " + digit_name (c/100) + " hundred";
		c = c%100;
	}
	if (c>= 20)
	{
		r = r + " " + tens_name (c/10);
		c = c %10;
	}
	else if (c >= 10)
	{
		r = r + " " + teen_name(c);
		c=0;
	}
	if (c>0)
	{
		r = r + " " + digit_name(c);
		return r;
	}
	
}
int main()
{
	int n;
	cout<< "Please enter a positive integer: ";
	cin >> n;
	cout << int_name(n);
	return 0;
}
