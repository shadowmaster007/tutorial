#include <iostream>
using namespace std;

bool is_nice(int n)
{
	while (n) // while n is not 0
	{
		int d = n % 10;
		if (d % 2 != 0) // checks whether the last digit can be divided by 2 without remainder
			return false;
		n /= 10; // removes the last digit from the number ( integer division )
	}
	return true;
}

int main()
{
	int n;
	while (cin >> n)
	{
		if (n < 2)
		{
			cout << "N/A" << endl;
		}
		else if (is_nice(n))
		{
			cout << "YES" << endl;
		}
		else
		{
			cout << "NO" << endl;
		}
	}

	return 0;
}
