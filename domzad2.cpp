/***
FN:F78864
PID:2
GID:1
*/

#include <iostream>
#include <limits>

using namespace std;

double function (int random_number)
{
   int N = random_number;
   int check = numeric_limits<int>::max();
      double digits = 0;
    while (random_number) 
	{
        random_number = random_number/10;
        digits++;
    }
  for (int i=0; i<digits; i++)
  {
    int numbers = N % 10;   
    N = (N - numbers);
    N = N/10;
    
    if (numbers<check)
	{
		 check=numbers;
	}
  }
	 return check;
}
int main()
{	
    int random_number;
	cin >> random_number;
	double lowest_digit = function(random_number);  
    cout << lowest_digit;
	return 0 ;
}



