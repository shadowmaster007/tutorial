#include <iostream>
using namespace std;
int main()
{
	cout << "Enter operation (+/-/x/:) ";
	string a;
	cin >> a;
	
	cout << "Enter operand 1: ";
	double b;
	cin >> b;
	
	cout << "Enter operand 2: ";
	double c;
	cin >> c;
	
	if (a == "+")
	{
		cout << b << " + " << c << " = " << b+c << endl;
	}
	if (a == "-")
	{
		cout << b << " - " << c << " = " << b-c << endl;
	}
	if (a == "x")
	{
		cout << b << " x " << c << " = " << b*c << endl;
	}
	if (a == ":")
	{
		cout << b << " : " << c << " = " << b/c << endl;
	}
	return 0;
}
