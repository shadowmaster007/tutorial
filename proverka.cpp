#include <iostream>
#include <cmath>
using namespace std;
int main()
{
	double area;
	cout << "Please enter the area of a square: ";
	cin >> area;
	if (cin.fail())
	{
		cout << "Error: Bad input " << endl;
		return 1;
	}	
	else if (area<0)
	{
		cout << "Error: Negative area " << endl;
		return 1;
	}
	
	cout << "The side lenght is " << sqrt(area) << endl;
	return 0;
	
}
