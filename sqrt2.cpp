#include <iostream>
#include <cmath>
using namespace std;
int main()
{
	double area;
	cout << "Enter the area of a square: ";
	cin >> area;
	if (area < 0)
	{
		cout << "Error! ";
		return 1;
	}
	else
	{
		double length = sqrt(area);
		cout <<  length << endl;
	}
	return 0;
}

