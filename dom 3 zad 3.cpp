/***
FN:F78864
PID:3
GID:1
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int a=0;
double b=0;

void add(vector<double> &First)
{
	cin >> a >> b;
	First.insert(First.begin() + (a-1),b);
}

void print(vector<double>&First)
{
	for(int i = 0; i<First.size(); i++)
	{
		
		if (i==First.size()-1)
			cout <<First[i]<< endl;
		
		else
		    cout <<First[i]<< " ";
	}
}

void remove(vector<double>&First)
{
	cin >> a;
	First.erase(First.begin()+ (a-1));
}

void shift_left(vector<double>&First)
{
	First.push_back(First[0]);
	First.erase(First.begin());
}

void shift_right(vector<double>&First)
{
	First.insert(First.begin(),First[First.size()]);
	First.erase(First.end()-1);
}

void commands(const string &A, vector<double> &First)
{

	if(A == "add")
	   add(First);
	else if (A == "print")
	   print(First);
	else if (A == "remove")
	   remove(First);
	else if (A == "shift_left")
	   shift_left(First);
	else if (A == "shift_right")
	   shift_right(First);
	
}

int main()
{
	vector<double> First(0);
	int F=0;
	string A;
	double N=0;
	cin >> F;
	for(int i = 0;i<F;i++)
	{
		cin >> N;
		First.push_back(N);
	}
	while(cin>> A)
	{
		commands(A,First);
		if(A == "exit")
		{
			break;
		}
	}

	return 0;
}
