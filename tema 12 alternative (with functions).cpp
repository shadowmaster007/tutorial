#include <iostream>
using namespace std;

string name;

int execute_set()
{
	cin >> name;
	return 0;
}

int execute_exit()
{
	cout << "Goodbye, " << name << "." << endl;
	return -1;
}

int execute_hello()
{
	cout << "Hello, " << name << "!" << endl;
	return 0;
}

int main()
{
	
	string cmd;
	while (cin >> cmd) //main loop
	{
		int resultCode = 0;
		if(cmd == "set")
		  resultCode = execute_set();
		else if (cmd == "hello")
		{
			resultCode = execute_hello();
		}
		else if (cmd == "exit")
		{
			resultCode = execute_exit();
		}
		else
		{
			cout << "Unrecognised command." << endl;
		}
		if(resultCode == -1)
		{
			break;
		}
	}
	return 0;
}
