#include <iostream>
#include <iomanip>
#include <limits>

using namespace std;

int main()
{
    int number;
    int maxNumber = numeric_limits<int>::min();
    int minNumber = numeric_limits<int>::max();
    while(cin >> number)
    {
        maxNumber = max(maxNumber, number);
        minNumber = min(minNumber, number);
    }
    cout << minNumber << " " << maxNumber << endl;
}



//version 1
        /*
        if(number > maxNumber)
            maxNumber = number;
        if(number < minNumber)
            minNumber = number;
        */

        //version 2

