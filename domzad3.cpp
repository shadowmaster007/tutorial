/***
FN:F78864
PID:3
GID:1
*/

#include <iostream>
using namespace std;

void grade_system(string number,double mark)
{
	string pass = "[PASS]";
	string fail = "[FAIL]";
    string invalid = "[INVALID]";
if (mark >= 3.00 && mark <= 6.00)
{
	cout << number <<"\t" << pass << endl;
}
else if (mark < 3.00 && mark >= 2.00)
{
	cout << number <<"\t" << fail << endl;
}
else 
{
	cout << number << "\t" << invalid << endl;
}
}
int main()
{
	string number;
	double mark;
    while(cin >> number >> mark)
    {
    	grade_system(number,mark);
    }
	return 0;
}
