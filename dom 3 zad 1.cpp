/***
FN:F78864
PID:1
GID:1
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

double multiply_and_add(vector<double> &First, vector<double> &Second)
{
	double sum = 0;
	for (int i=0; i<First.size();i++)
	{
		sum += First[i]*Second[i];
	}
	return sum;
}


int main()
{

	vector<double> First(0);
	vector<double> Second (0);
	int F = 0;
	double n=0.00;	
	cin >> F;
	for (int i=0; i<F; i++)
	{
		cin >> n;
		First.push_back(n);
	}
	for (int i=0; i<F; i++)
	{
	    cin >>n;
		Second.push_back(n);	
	}	
	double result = multiply_and_add(First,Second);
	
	cout << result << endl;

	return 0;
}
