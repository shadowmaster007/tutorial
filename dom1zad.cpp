/***
FN:F78864
PID:1
GID:1
*/

#include <iostream>
#include <limits>
using namespace std;

int main()
{
	int N;
	double next=0;
	double sum =0;
	cin >> N;
	for (int i=0; i<N; i++)
	{
		if (cin>>next)
		{
			sum= sum + next;
		}
		
	}
	cout<< "The sum is " << sum << endl;
	return 0;
}
	
	
/**  
using namespace std;

double function_n ( int N, double next, double sum)      // SAME TASK, BUT USING FUNCTIONS
{
	cin >> N;
	for (int i=0; i<N; i++)
	{
		
		if (cin>>next)
		{
			sum= sum + next;	
		}
	}
	return sum;
}

int main()
{
	int N;
	double next =0;
	double sum =0;
	double f_next = function_n (N,next,sum);
	cout<< "The sum is " << f_next;
	return 0;
} 
*/ 
