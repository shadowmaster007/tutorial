#include <iostream>
using namespace std;
int main()
{
	cout << "How many pennies do you have?";
	int pennies;
	cin >> pennies;
	
	cout << "How many dimes do you have?";
	int dimes;
	cin >> dimes;
	
	cout << "How many quarters do you have?";
	int quarters;
	cin >> quarters;
	
	int value = pennies + dimes*10 + quarters*25;
	int dollar = value/100;
	double cents = value % 100;
	cout << "Total value = " << dollar << " dollars and " << cents << " cents. " << endl;
	return 0;
}
