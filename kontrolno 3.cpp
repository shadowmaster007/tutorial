/***
FN:F78864
PID:1
GID:1
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

double add_and_multiply(vector<double> &First, vector<double> &Second)
{
	double Result = 0;
	for (int i = 1; i<First.size(); i++)
	{
		if (i%2)
		{
			Result = Result + First[i]*Second[i];
		}
	}
	return Result;
}

int main()
{
	vector<double> First(0);
	vector<double> Second(0);
	int F = 0;
	cin >> F;
	double n = 0;
	
	for (int i=0; i<F; i++)
	{
		cin >> n;
		First.push_back(n);
	}
	for (int i=0; i<F; i++)
	{
		cin >> n;
		Second.push_back(n);
	}
	
	double result = add_and_multiply(First,Second);
	
	cout << result << endl;

	return 0;
}
