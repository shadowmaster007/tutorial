#include <iostream>
using namespace std;
int main()
{
	cout << "Enter numbers in meters: ";
	double a;
	cin >> a;
	
	double b = a/1000;
	double c = a*10;
	double d = a*100;
	
	cout << a << "m = " << b << "km" << endl;
	cout << a << "m = " << c << "dm" << endl;
	cout << a << "m = " << d << "cm" << endl;
	return 0;
}
